#!/usr/bin/env python3

from flask import Flask, render_template
from flask_restful import Api
from flask_cors import CORS
import os


app = Flask(__name__)
api = Api(app)
# just for localhost testing, disable in production
CORS(app)

__version__ = "1.1"
path = 'data/persistent.txt'


def getHost():
  #return socket.getfqdn()
  return os.uname()[1]


def createDir(path,dirmode=0o666):       
  dr = os.path.dirname(path)
  try:
    if os.path.exists(dr):
      return True
    else:
      os.mkdir(path=dr,mode=dirmode)
      return True
  except OSError as oserr:
      print("An error occurred while creating folder "+str(oserr))
      return False

# write the content in a text file
def writeFile(text, path):
  try:
    if not os.path.exists(path):
      created = createDir(path)
      if not created:
        return False
    with open(path,"a") as wfile:
        wfile.write(text+"\n<br/>")
        return True
  except Exception as e:    
    print("An error occurred while trying to write "+str(text)+ ": "+str(e)+".")
    return False

# read a file
def readFile(toRead):
  if os.path.exists(toRead):
    try:
      with open(toRead, 'r') as content:
        data=content.read()
    except Exception as e:
      print("An error occurred while reading the "+ str(toRead)+"file: "+str(e))
      return False
    return str(data)

# root endpoint
@app.route("/")
def helloWorld():
  msg = "Useless app v."+__version__+"\n<br/>"
  podName = getHost()
  writeFile(podName, path)
  fl = readFile(path)
  msg = msg + "Hello from host "+podName+"\n<br/>"
  if fl:
    msg = msg + fl
  return msg


if __name__=="__main__":
  app.run(host='0.0.0.0')
